// The Mana World script
// Author: Jesusalva <jesusalva@themanaworld.org>
//
// Magic Script: SKILL_LUM (Level 1)
// School: Life 1

function	script	SK_Lum	{
    // In some cases it is... aborted
    if (getunittype(@skillTarget) == UNITTYPE_PC) {
        .@me=getcharid(3);
        .@ok=true;
        attachrid(@skillTarget);
        // Kill the GM Event
        if (isequipped(MagicGMTopHat))
            .@ok=false;
        // Ailments cannot be bypassed nor healed until Lv 3 Life Magic
        if (getstatus(SC_BLOODING) ||
            getstatus(SC_HALT_REGENERATION) ||
            getstatus(SC_CURSE))
            .@ok=false;
        if (getstatus(SC_POISON) && !getstatus(SC_SLOWPOISON))
            .@ok=false;
        if (getstatus(SC_DPOISON) && !getstatus(SC_SLOWPOISON))
            .@ok=false;
        // Already dead
        if (Hp < 1)
            .@ok=false;
        // Finished
        .@limit=MaxHp-Hp;
        detachrid();
        attachrid(.@me);
        if (!.@ok) return;
    } else {
        if (getunitdata(@skillTarget, UDT_HP) < 1) return;
        .@limit=getunitdata(@skillTarget, UDT_MAXHP)-
                getunitdata(@skillTarget, UDT_HP);
    }

    // No need for healing? Otherwise, take reagent
    if (.@limit <= 0) return;
    delitem Lifestone, 1;

    // Real healing happens here
    .@PW=60+(10*@skillLv);
    .@dmg=AdjustSpellpower(.@PW);
    sc_start(SC_M_LIFEPOTION, 5000, 1+max(0, .@dmg/5), 10000,
             SCFLAG_NOAVOID|SCFLAG_FIXEDTICK|SCFLAG_FIXEDRATE, @skillTarget);
    specialeffect(FX_MAGIC_WHITE, AREA, @skillTarget);
    if (@skillTarget != getcharid(3))
        specialeffect(FX_MAGIC_WHITE, AREA, getcharid(3));

    // Specifics
    if (getskilllv(SKILL_MAGIC_DARK) >= 1)
        SC_Bonus(2, SC_HALT_REGENERATION, 1);

    // Gives EXP according to how much you healed
    if (@skillTarget != getcharid(3))
        setq2(MagicQuest_Healing, getq2(MagicQuest_Healing)+1);
    getexp min(.@dmg, .@limit)*getskilllv(SKILL_MAGIC_LIFE), .@PW/10;
    GetManaExp(@skillId, 1);
    return;
}

