// The Mana World script
// Author: Jesusalva <jesusalva@themanaworld.org>
//
// Magic Script: Multiple (Level 1)
// School: Nature 1

function	script	SK_GrowPlants	{
    // Setup
    switch (@skillId) {
    case SKILL_MODRIPHOO:
        .@it = AlizarinHerb; .@mobId = AlizarinPlant; break;
    case SKILL_MODRISUMP:
        .@it = CobaltHerb; .@mobId = CobaltPlant; break;
    case SKILL_MODRIYIKAM:
        .@it = GambogeHerb; .@mobId = GambogePlant; break;
    case SKILL_MODRILAX:
        .@it = MauveHerb; .@mobId = MauvePlant; break;
    default: return;
    }
    // Consume reagents
    delitem Root, 1;
    delitem .@it, 1;
    // Continue but with a special flag
    SK_summon(.@mobId, 2, 1, false);
    return;
}

