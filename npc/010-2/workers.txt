
010-2,23,70,0	script	Josh	NPC155,{
    mes "[Josh]";
    mes "\"We're working on getting the cellar pass open.\"";
    close;
}

010-2,39,75,0	script	Zack	NPC155,{
    @halloween_npc_id = $@halloween_npc_zack;
    callfunc "TrickOrTreat";

    mes "[Zack]";
    mes "\"My brother and I are fixing the cellar pass.\"";
    close;
}
