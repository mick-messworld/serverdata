// The Mana World Script
// (C) Jesusalva, 2021
// Licensed under GPLv2 or later

006-2,81,36,0	script	Reathe	NPC179,{
    shop .name$;
    goodbye;
    close;

OnInit:
    tradertype(NST_ZENY);
    sellitem ToySabre;
    sellitem Cap;
    sellitem HighPriestCrown;
    sellitem MonsterSkullHelmet;
    .distance = 5;
    end;
}

006-2,30,35,0	script	Ardra	NPC179,{
    shop .name$;
    goodbye;
    close;

OnInit:
    tradertype(NST_ZENY);
    sellitem CactusDrink;
    sellitem CactusPotion;
    sellitem BottleOfWater;
    sellitem RoastedMaggot;
    .distance = 5;
    end;
}

