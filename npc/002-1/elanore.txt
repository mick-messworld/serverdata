
002-1,75,62,0	script	Elanore#_M	NPC108,{
    @has_magic = getskilllv(SKILL_MAGIC);

    @Q_status = getq(MagicQuest_Healing);
    .@heal_exp = getq2(MagicQuest_Healing);

    // Set up SkillUp function
    @SUP_id = SKILL_MAGIC_LIFE;
    @SUP_name$ = "Life Magic";

    if (sc_check(SC_POISON) || sc_check(SC_DPOISON)) goto L_CurePoison;
    if (BaseLevel > 20) goto L_NoHeal;
    mesn l("Elanore the Healer");
    mesq any(
            l("You don't look too well; let me treat your wounds."),
            l("I will make quick work of your wounds."),
            l("Need a healing?"),
            l("Sometimes you just need to run from battle."));
    next;
    mesn l("Elanore the Healer");
    mesq any(
            l("Here you go!"),
            l("Painless, wasn't it?"),
            l("You should be more careful."),
            l("Much better, right?!"));
    percentheal 100, 100;
    goto L_close;

L_NoHeal:
    if (MAGIC_FLAGS)
        goto L_Chat;
    goto L_NoHealMessage;

L_NoHealMessage:
    mesn l("Elanore the Healer");
    mes "\"I'm sorry but unless you are young and in need, I can't help you, and your level is already higher than twenty.";
    mes "With so many gaurds still injured, I really have no time to help.";
    mes "You can get some rest in the inn near here.\"";
    goto L_close;

L_Chat:
    mesn l("Elanore the Healer");
    mesq l("Hello! Can I help you?");
    next;
    goto L_Main;

L_Main:
    if (@has_magic && (@Q_status == .ST_INITIAL))
        menu
            "Can you heal me?", L_NoHealMessage,
            "Can you teach me magic?", L_Teach,
            "What do you know about...", L_Question,
            "Goodbye!", L_Bye;
    if (@has_magic && (@Q_status > .ST_INITIAL))
        menu
            "Can you heal me?", L_NoHealMessage,
            "Can you teach me more?", L_Teach,
            "Where can I get more lifestones?", L_Lifestones,
            "What do you know about...", L_Question,
            "Goodbye!", L_Bye;
    if (!@has_magic)
        menu
            "Can you heal me?", L_NoHealMessage,
            "What do you know about...", L_Question,
            "Goodbye!", L_Bye;
    goto L_close;

L_Lifestones:
    mesn l("Elanore the Healer");
    mesq l("I always need components for my own healing spells. If you bring me ten gamboge leaves, ten alizarin leaves, ten mauve leaves and ten cobalt leaves, I can make ten lifestones; I will give you half of them.");
    next;
    menu
        "OK, I will get them.", L_close,
        "No way!", L_close,
        "Here you are!", L_Trade,
        "Can I make them myself?", L_MakeSelf;

L_Trade:
    if (countitem(MauveHerb) < 10) goto L_Trade_Missing;
    if (countitem(GambogeHerb) < 10) goto L_Trade_Missing;
    if (countitem(AlizarinHerb) < 10) goto L_Trade_Missing;
    if (countitem(CobaltHerb) < 10) goto L_Trade_Missing;
    getinventorylist;
    if (@inventorylist_count == 100 && countitem(Lifestone) == 0
        && countitem(MauveHerb) > 10
        && countitem(GambogeHerb) > 10
        && countitem(AlizarinHerb) > 10
        && countitem(CobaltHerb) > 10) goto L_NoRoom;

    delitem MauveHerb, 10;
    delitem GambogeHerb, 10;
    delitem AlizarinHerb, 10;
    delitem CobaltHerb, 10;
    getitem Lifestone, 5;
    getexp 200, 1;
    mesn l("Elanore the Healer");
    mesc l("Elanore carefully goes over your leaves, then smiles.");
    mesq l("These will do just fine.");
    next;
    mesc l("She picks up all forty and presses them together in her hands, then whispers something.");
    next;
    mesn l("Elanore the Healer");
    mesc l("As she opens her hands again, the leaves have turned into ten golden crystals.");
    mesq l("Five for you and five for me.");
    mesc l("She smiles.");
    mes "[200 experience points]";
    goto L_close;

L_NoRoom:
    mesn l("Elanore the Healer");
    mesq l("Oh... it seems that you have no room for the lifestones. But please do come back later when you have made a little space.");
    goto L_close;

L_Trade_Missing:
    mesn l("Elanore the Healer");
    mesc l("Elanore counts the leaves you show her.");
    mesq l("No, I am sorry. Please bring me ten leaves each of gamboge, alizarin, mauve, and cobalt.");
    goto L_close;

L_MakeSelf:
    mesn l("Elanore the Healer");
    if (@has_magic < 2) goto L_MakeSelf_no;
    goto L_MakeSelf_yes;

L_MakeSelf_yes:
    mesq l("Oh, but of course! You are powerful enough to make your own lifestones, using the enchantment '%s'. This will consume a bug leg, a maggot slime and one of each of the four healing herbs, though.", b("Manpahil"));
    learnskill SKILL_MANPAHIL;
    goto L_close;

L_MakeSelf_no:
    mesq l("No, you are not powerful enough yet. Please come back and ask me again when you have learned to control more powerful magic, though!");
    goto L_close;

L_Teach:
    if (@Q_status == .ST_INITIAL) goto L_T_Initial;
    if (@Q_status == .ST_LEARNED_LIGHT_HEAL) goto L_T_ChkAdvTo2;
    if (@Q_status == .ST_MASTERED_LIGHT_HEAL) goto L_T_ChkAdvToLOH;
    if (@Q_status == .ST_LEARNED_LAYONHANDS) goto L_T_Explain3;
    if (@Q_status == .ST_WAITING_FOR_KADIYA) goto L_T_ChkAdvTo3;
    if (@Q_status == .ST_READY_TO_CURE_POISON) goto L_T_CurePosion;
    if (@Q_status == .ST_LEARNED_CURE_POISON) goto L_T_dunno;

    mesn l("Elanore the Healer");
    mesq l("You have made good progress, but you lack the magical power to advance further. Also, I would like to observe you some more to be certain that you will make a good healer.");
    goto L_close;

L_T_Initial:
    mesn l("Elanore the Healer");
    mesq l("I will only teach you magic of the school of Life, and that only if you prove yourself to be a good healer – that is, if you use your powers to help others.");
    mesc l("She hesistates.");
    next;
    mesn l("Elanore the Healer");
    mes "\"I don't know you very well, but I don't think that there is any harm in teaching you a simple spell.\"";
    mes "She reaches inside her satchel and pulls out a piece of crystal, glittering in the sun.";
    mes "\"This here is a lifestone, life energy encased in a crystal shell. You will need it for most healing magic.\"";
    next;
    mesn l("Elanore the Healer");
    mesq l("To heal someone, first locate the injury. As a beginner, you have to touch the wound; with practice, it will be enough to think about it. Hold the lifestone in one hand, touching the wound with the other.");
    mesq l("Then chant the lesser healing spell, '%s', with whoever you wish to heal targeted.", b("lum"));
    next;
    mesn l("Elanore the Healer");
    mesq l("This will only cure cuts and bruises, though, and it will take some time to take effect. It will be useless to mend broken bones or more severe injuries!");
    mesq l("To heal yourself, it's enough to just cast by itself, without a target selected.");
    next;
    mesn l("Elanore the Healer");
    mesq l("Let me give you a lifestone to get started with.");
    next;
    getinventorylist;
    if ((@inventorylist_count == 100) && (countitem(Lifestone) == 0)) goto L_T_Initial_Noroom;
    getitem Lifestone, 1;
    learnskill SKILL_LUM;
    setq1(MagicQuest_Healing, .ST_LEARNED_LIGHT_HEAL);

    mesn l("Elanore the Healer");
    mes "Elanore hands you the crystal.";
    mes "\"Here you are. I hope that it will allow you to do good!\"";
    goto L_close;

L_T_Initial_Noroom:
    mesn l("Elanore the Healer");
    mesq l("Oh... you managed to put things into every little nook and pocket, haven't you? Well, come back later, I will give it to you then.");
    goto L_close;

L_T_ChkAdvTo2:
    mesn l("Elanore the Healer");
    if (.@heal_exp < 20) goto L_T_ChkAdvTo2_fail;
    mesc l("Elanore smiles at you.");
    mesq l("From what I have been hearing, you seem to be doing well on your way to becoming a healer. I have decided to accept you as a student of the School of Life Magic.");
    next;
    if (getskilllv(SKILL_MAGIC) < 2) {
        mesn l("Elanore the Healer");
        mesq l("But be warned: you won't be able to use advanced healing magic until you are more capable in your regular magic usage.");
        next;
    }
L_T_AdvTo2_skip:
    mesn l("Elanore the Healer");
    mesq l("Please stand still.");
    mesc l("She walks up to you, holds your shoulders, and smiles at you.");
    next;
    mesn l("Elanore the Healer");
    mesc l("Then, she rises up on her tiptoes and kisses you on your forehead.");
    next;
    mesn l("Elanore the Healer");
    mesc l("You feel a strange, tingling kind of warmth spread through your body.");
    setq1(MagicQuest_Healing, .ST_MASTERED_LIGHT_HEAL);
    @SUP_xp = 5000;
    @SUP_lvl = 2;
    callfunc "SkillUp";
    goto L_close;

L_T_ChkAdvTo2_fail:
    mesq l("I think I would like to observe you for a little longer to see if you would make a good healer.");
    if (.@heal_exp < 5)
        mesq l("You are still at the beginning of your path, but do keep practicing.");
    if ((.@heal_exp >= 5) && (.@heal_exp < 15))
        mesq l("You are making progress, but you still need more practice.");
    if (.@heal_exp >= 15)
        mesq l("You have made good progress, but please do continue in your efforts.");
    goto L_close;

L_T_ChkAdvToLOH:
    if (getskilllv(SKILL_MAGIC) < 2) goto L_T_LOH_adv_abort0;
    if (.@heal_exp < 30) goto L_T_LOH_adv_abort1;

    mesn l("Elanore the Healer");
    mesq l("I will now teach you how to heal by laying on your hands. The technique is similar to the spell I taught you at the beginning, but this time you transfer your own health instead of drawing health from a lifestone.");
    next;
    mesn l("Elanore the Healer");
    mesq l("\"First, lay your hand on the person you wish to heal. You needn't touch the injury itself, though you have to touch the skin until you are a little more experienced. Then, medidate on the word '%s', which is the lay on hands magic.\"", b("inma"));
    next;
    mesn l("Elanore the Healer");
    mesq l("This will let your own life force flow into the person you are healing. If you are badly injured yourself, you will not be able to do this and may even injure yourself further.");
    next;
    mesn l("Elanore the Healer");
    mesq l("Unlike lesser heal, it'll heal instantly, however it still cannot help if the person has a serious injury or has been inflicted with an ailment.");
    mes "[1000 experience points]";
    getexp 1000, 0;
    learnskill SKILL_INMA;
    setq1(MagicQuest_Healing, .ST_LEARNED_LAYONHANDS);
    goto L_close;

L_T_LOH_adv_abort0:
    mesn l("Elanore the Healer");
    mesq l("You don't have enough magical power to learn the next healing spell yet.");
    goto L_close;

L_T_LOH_adv_abort1:
    mesn l("Elanore the Healer");
    mesq l("I'm sorry, but I am not yet convinced that you are a good enough healer. Please continue in your endeavors.");
    goto L_close;

L_T_Explain3:
    if (getskilllv(SKILL_MAGIC) < 3) goto L_T_Explain3_abort;
    if (.@heal_exp < 50) goto L_T_Explain3_fail;

    mesn l("Elanore the Healer");
    mesc l("Elanore smiles.");
    mesq l("You seem to be ready to advance as a healer. There is one favor I must ask of you first, though.");
    set QUEST_MAGIC2, (QUEST_MAGIC2 & ~(NIBBLE_3_MASK)); // Initialise Kadiya quest
    setq1(MagicQuest_Healing, .ST_WAITING_FOR_KADIYA);
    @Q_status = getq(MagicQuest_Healing);
    next;

    mesn l("Elanore the Healer");
    mes "\"There is a man here in Tulimshar with a beautiful young daughter; his name is Omar, and his daughter's name is Kadiya.\"";
    mes "\"Once, when I was a young apprentice, I made a mistake treating Omar. Ever since, he no longer trusts me.\"";
    next;

    mesn l("Elanore the Healer");
    mes "\"But his daugher has contracted Mopox, so she needs help. He doesn't trust me, but perhaps he will trust you; you have been helping quite a few people, after all.\"";
    next;
    goto L_3_menu;

L_3_menu:
    menu
        "What should I do?", L_3_what_to_do,
        "Where can I find the sick girl?", L_3_where,
        "How did you mess up?", L_3_messup,
        "I will get started right away.", L_Main,
        "Maybe later.", L_Main;

L_3_what_to_do:
    mesn l("Elanore the Healer");
    mes "\"You will have to brew a Mopox cure potion and give it to her. You should ask Caul to help you, he knows the recipe.\"";
    next;
    mesn l("Elanore the Healer");
    mes "\"First, you will need a concentration potion and an iron potion – together, they will give her her strength back. You also need a lifestone, and a gamboge leaf against the toxins in her body.\"";
    next;
    mesn l("Elanore the Healer");
    mes "\"You then have to add in roots, water, mauve leaves and sulphur powder until the brew is gooey and neither light nor dark. Caul will explain the details to you.\"";
    next;
    mesn l("Elanore the Healer");
    mes "\"There is a transmutation spell to make sulphur powder. If you don't know the magic, perhaps you can ask a transmutation mage to help you with this part.\"";
    next;
    goto L_3_menu;

L_3_messup:
    mesn l("Elanore the Healer");
    mes "\"Ah... well, Omar is a trader, and a frequent traveler. One day he came back from a journey with fleas in his beard and asked me to get rid of them.\"";
    next;
    mesn l("Elanore the Healer");
    mes "\"Unfortunately I used the wrong lotion on the fleas, so instead of being scared away, they grew to enormous proportions and ate up all of his beard.\"";
    next;
    mesn l("Elanore the Healer");
    mes "Elanore blushes.";
    mes "\"Ever since then he hasn't been very trusting towards my skills, and I suppose that I can't blame him. But I don't want poor Kadiya to suffer because of that.\"";
    next;
    goto L_3_menu;

L_3_where:
    mesn l("Elanore the Healer");
    mes "\"Kadiya and Omar live relatively close by. See this corner in the inner city wall east of here? Turn past it, then go north. It's the second house.\"";
    next;
    goto L_3_menu;

L_T_Explain3_abort:
    mesn l("Elanore the Healer");
    mes "\"I'm sorry, but you lack the magical power to advance.\"";
    goto L_close;

L_T_Explain3_fail:
    mesn l("Elanore the Healer");
    if (.@heal_exp < 30)
        mes "\"You seem to have regressed as a healer. This is worrisome; please mend your ways or I will not teach you any further.\"";
    if ((.@heal_exp >= 30) && (.@heal_exp < 40))
        mes "\"You still need more practice in the art of healing.\"";
    if ((.@heal_exp >= 40))
        mes "\"You are doing well, but you need some more practice as a healer first.\"";
    goto L_close;

L_T_ChkAdvTo3:
    callfunc "KadiyaSubquestConsts";

    if (@Q_kadiya_status >= @Q_STATUS_COMPLETED) goto L_T_AdvTo3Ready;

    mesn l("Elanore the Healer");
    mes "\"Please help Kadiya.\"";
    next;
    goto L_3_menu;

L_T_AdvTo3Ready:
    mesn l("Elanore the Healer");
    mes "\"I have heard of what you did. Hiding the potion in her favorite food – that was clever.\"";
    mes "She smiles at you.";
    mes "\"Omar let me have a look at her earlier, and she seems to be recovering. You did very well.";
    next;
    mesn l("Elanore the Healer");
    mes "Elanor turns towards you, rises up on her toes and kisses you on the forehead.";
    next;
    @xp = 30000;
    if (@Q_kadiya_status == @Q_STATUS_COMPLETED_ELANORE)
        @xp = 50000;
    mesn l("Elanore the Healer");
    mes "You feel a strange, tingling kind of warmth spread through your body.";
    mes "[" + @xp + " experience points]";
    mes "[Level 3 in Life Magic]";

    setq1(MagicQuest_Healing, .ST_READY_TO_CURE_POISON);
    @Q_status = getq(MagicQuest_Healing);
    getexp @xp, 0;
    if (getskilllv(SKILL_MAGIC_LIFE) < 3)
        updateskill SKILL_MAGIC_LIFE, 3;
    next;

    if (@Q_kadiya_status != @Q_STATUS_COMPLETED_ELANORE) goto L_Main;

    mesn l("Elanore the Healer");
    mes "\"Overall I am impressed by your progress. If you continue as you are doing now, you might become a great healer.\"";
    mes "She hands you a handful of lifestones.";
    mes "\"Please come back soon; there is more that I would like to teach you.\"";
    next;
    MAGIC_FLAGS = MAGIC_FLAGS | MFLAG_ELANORE_OMAR;
    getitem Lifestone, 20;

    goto L_Main;

L_T_CurePosion:
    mesn l("Elanore the Healer");
    mes "\"Thank you again for your help with Kadiya!\"";
    mes "\"Curing sicknesses is often quite difficult; most of them require complicated potions to be brewed.\"";
    next;
    mesn l("Elanore the Healer");
    mes "\"Fortunately, things are much easier with most poisons. For those we have a simple spell that only requires a Gamboge leaf.\"";
    mes "\"This is the next spell that I would like to teach you.\"";
    next;
    mesn l("Elanore the Healer");
    mes "\"If you would like to cure someone who is poisoned, first rub a Gamboge leaf between your hands. Your hands must be covered in Gamboge liquid for this to work.\"";
    mes "\"Speak the invocation, `" + get(.invocation$, "cure-poison") + "'.\"";
    mes "\"Next, you either touch the poisoned person with your hands, or speak their name. You have to be close for this to work, though.\"";
    next;
    mesn l("Elanore the Healer");
    mes "\"Once again, the invocation is `" + get(.invocation$, "cure-poison") + "'.\"";
    mes "\"Come back again soon; there is another spell I would like to teach you.\"";
    next;
    setq1(MagicQuest_Healing, .ST_LEARNED_CURE_POISON);
    @Q_status = getq(MagicQuest_Healing);
    goto L_Main;

L_T_dunno:
    mesn l("Elanore the Healer");
    mesq l("Hmm. I wanted to teach you something, but I've forgotten what it was... please come back later.");
    next;
    goto L_Main;

L_Question:
    callfunc "MagicTalkOptionsSetup";
    @ignore = @QQ_ELANORE;
    callfunc "MagicTalkMenu";

    if (@c == 0) goto L_Main;
    if (@c == @QQ_MANASEED) goto L_Q_manaseed;
    if (@c == @QQ_MANAPOTION) goto L_Q_manapotion;
    if (@c == @QQ_WYARA) goto L_Q_wyara;
    if (@c == @QQ_SAGATHA) goto L_Q_sagatha;
    if (@c == @QQ_AULDSBEL) goto L_Q_auldsbel;
    if (@c == @QQ_OLDWIZ) goto L_Q_oldwiz;
    if (@c == @QQ_ASTRALSOUL) goto L_Q_astralsoul;

    mesn l("Elanore the Healer");
    mesc l("Elanore shakes her head.");
    mesq l("I am sorry, but I don't think that I know anything that would help you.");
    next;
    goto L_Main;

L_Q_astralsoul:
    mesn l("Elanore the Healer");
    mes "\"The astral soul skill is one of the most important skills for mages. I learned this focus long time ago. Without that skill your magic is quite useless, isn't it? If the rumors are really true, there is an experienced mage in the Woodlands around Hurnscald.\"";
    next;
    goto L_Main;

L_Q_oldwiz:
    mesn l("Elanore the Healer");
    mesq l("He is a kind old man. He stays mostly with his books and his apprentice, though we have chatted a few times.");
    next;
    goto L_Main;

L_Q_manaseed:
    mesn l("Elanore the Healer");
    mesc l("She smiles.");
    mesq l("If the rumors are true and there really is a Mana Seed, then that would be wonderful news – perhaps we will have a few more healers soon!");
    next;
    goto L_Main;

L_Q_manapotion:
    mesn l("Elanore the Healer");
    mesq l("I'm sorry, but I can't help you with that.");
    next;
    goto L_Main;

L_Q_wyara:
    mesn l("Elanore the Healer");
    mesq l("Wyara is the Hurnscald town witch. She was also the town healer before the Doctor moved there.");
    next;
    goto L_Main;

L_Q_sagatha:
    mesn l("Elanore the Healer");
    mesq l("Oh, I have only met Sagatha once or twice, while collecting herbs. She is a warm and gentle person, but she tries to hide it.");
    next;
    goto L_Main;

L_Q_auldsbel:
    mesn l("Elanore the Healer");
    mesq l("Oh, sorry – I don't really know him very well.");
    next;
    goto L_Main;

L_CurePoison:
    mesn l("Elanore the Healer");
    mesq l("Oh dear! That looks like poison; hang on...");
    mesc l("She performs a complicated gesture.");
    sc_end(SC_POISON);
    sc_end(SC_DPOISON);
    sc_end(SC_SLOWPOISON);
    misceffect sfx_magic_life, strcharinfo(0);
    misceffect sfx_magic_life;
    next;
    mesn l("Elanore the Healer");
    mesq l("There you are, right as rain! Now you take care, all right?");
    goto L_close;

L_Bye:
    mesn l("Elanore the Healer");
    mesq l("Goodbye!");
    goto L_close;

L_close:
    @has_magic = 0;
    @Q_status = 0;
    @SUP_id = 0;
    @SUP_name$ = "";
    @ignore = 0;
    @xp = 0;
    close;

OnInit:
    .ST_INITIAL = 0;
    .ST_LEARNED_LIGHT_HEAL = 1;
    .ST_MASTERED_LIGHT_HEAL = 2;
    .ST_LEARNED_LAYONHANDS = 3;
    .ST_WAITING_FOR_KADIYA = 4;
    .ST_READY_TO_CURE_POISON = 5;
    .ST_LEARNED_CURE_POISON = 6;
    end;
}
