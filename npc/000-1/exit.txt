// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    Special Soul Menhir which only allows leaving the map.

000-1,22,22,0	script	Emergency Exit	NPC_NO_SPRITE,2,2,{
OnTouch:
OnTalk:
OnTalkNearby:
    if (LOCATION$ != "") goto L_ToLoc;
    if (Sex)
        warp "029-2", 25, 24;
    else
        warp "029-2", 22, 24;
    end;
L_ToLoc:
    // Possibly could warp to "Save" as well?
    ReturnTown();
    end;
}

