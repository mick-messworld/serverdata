// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 018-2: Woodland Mining Camp Indoor warps
018-2,50,29,0	warp	#018-2_50_29	0,0,018-1,65,74
018-2,41,20,0	warp	#018-2_41_20	0,0,018-1,64,69
018-2,20,24,0	warp	#018-2_20_24	0,0,018-2,112,26
018-2,112,25,0	warp	#018-2_112_25	0,0,018-2,20,23
018-2,21,28,0	warp	#018-2_21_28	1,0,018-2,79,25
018-2,79,26,0	warp	#018-2_79_26	1,0,018-2,21,29
018-2,114,19,0	warp	#018-2_114_19	0,0,018-3,177,55
