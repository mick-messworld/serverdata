
018-2,31,39,0	script	Angus	NPC147,{
    @inspector = ((QUEST_Hurnscald & NIBBLE_3_MASK) >> NIBBLE_3_SHIFT);

    if (@inspector == 10) goto L_NohMask;

    mes "[Angus]";
    mes "Angus is tinkering with some mechanical concoction.";
    mes "\"It keeps breakin' left an' right... fortunat'ly I put in redundancies everywhere, but I haf' to keep repairin'.\"";
    goto L_close;

L_NohMask:
    mes "[Angus]";
    mes "\"I'm sorry, I truely am, but I stay in the town. One o' the miners might have heard something.\"";
    goto L_close;

L_close:
    @inspector = 0;
    close;
}
