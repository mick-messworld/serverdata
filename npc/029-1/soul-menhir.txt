
029-1,39,101,0	script	Soul Menhir#candor	NPC344,{
    @map$ = "029-1";
    setarray @Xs, 38, 39, 40, 38, 40, 38, 39, 40;
    setarray @Ys, 100, 100, 100, 101, 101, 102, 102, 102;
    @x = 0;
    @y = 0;
    callfunc "SoulMenhir";
    @map$ = "";
    cleararray @Xs[0], 0, getarraysize(@Xs);
    cleararray @Ys[0], 0, getarraysize(@Ys);
    @x = 0;
    @y = 0;
    close;
}
