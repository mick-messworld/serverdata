
-	script	#hideandseekNTconfig	NPC32767,{
    end;

OnInit:
    $@hideandseekNT_started = (1 << 0);
    $@hideandseekNT_found1 = (1 << 1);
    $@hideandseekNT_found2 = (1 << 2);
    $@hideandseekNT_found3 = (1 << 3);
    $@hideandseekNT_found4 = (1 << 4);
    $@hideandseekNT_found5 = (1 << 5);
    $@hideandseekNT_ended = (1 << 6);
    end;
}

function	script	CountHideNSeek	{
    .@q=getq2(CandorQuest_HideNSeek);
    setq3 CandorQuest_HideNSeek, bitmask_count(.@q);
    return getq3(CandorQuest_HideNSeek);
}

function	script	UpdateHideNSeek	{
    setq2 CandorQuest_HideNSeek, getq2(CandorQuest_HideNSeek) | getarg(0);
    setq3 CandorQuest_HideNSeek, bitmask_count(getq2(CandorQuest_HideNSeek));
    return;
}

029-1,64,90,0	script	AyashaDebug	NPC258,{
    .@q=getq2(CandorQuest_HideNSeek);
    mes "[Ayasha Debug]";
    menu
        "Show", L_Show,
        "Reset", L_Reset,
        "Close.", -;
    close;

L_Show:
    mes "State: " + .@q;
    if (.@q & $@hideandseekNT_started)
        mes "Start set";
    if (.@q & $@hideandseekNT_found1)
        mes "Found 1 set";
    if (.@q & $@hideandseekNT_found2)
        mes "Found 2 set";
    if (.@q & $@hideandseekNT_found3)
        mes "Found 3 set";
    if (.@q & $@hideandseekNT_found4)
        mes "Found 4 set";
    if (.@q & $@hideandseekNT_found5)
        mes "Found 5 set";
    if (.@q & $@hideandseekNT_ended)
        mes "Complete set";
    close;

L_Reset:
    setq CandorQuest_HideNSeek, 0, 0;
    close;

OnInit:
    if (!debug)
        disablenpc "AyashaDebug";
    end;
}

029-1,62,90,0	script	Ayasha	NPC258,{
    @child_number = 0;
    callfunc "XmasList";

    .@q=getq2(CandorQuest_HideNSeek);

    if (.@q > 63) goto L_Done;
    if (.@q == 63) goto L_Reward;
    if (getq(CandorQuest_HideNSeek)) goto L_Doing;

    mes l("You see a girl who is holding her hands in front of her face.");
    mesn;
    mesq l("98... 99... 100!");
    mesc l("She takes away her hands and looks up.");
    menu
        "Hello, what are you doing?",L_Next,
        "(Leave)",-;
    close;

L_Next:
    mesn;
    mes "\"I'm playing hide and seek with my friends!";
    mes "Do you want to play with us?\"";
    menu
        "Sure, sounds like fun.",L_Next1,
        "I'm busy with more important things!",-;
    close;

L_Next1:
    mesn;
    mes "\"Great! You can take my turn with searching. You need to find all five of my friends.";
    mes "They're hiding somewhere on the island, you will have to explore to find them. They could be hiding anywhere.\"";

    setq CandorQuest_HideNSeek, 1, 0, 0;
    close;

L_Doing:
    mesn;
    mesq l("You still need to find some of my friends. You have found %d out of 5 of my friends.", CountHideNSeek());
    mesq l("Remember, they're hiding somewhere on the island, you will have to explore to find them. They could be hiding anywhere.");
    close;

L_Reward:
    mesn;
    mesq l("Oh, you found them all! Good job! Now it's my turn with searching again. Go and hide!");
    getexp 75, 0;
    setq CandorQuest_HideNSeek, 2, 127, 0;
    menu
        "I think I need to do other things now.",L_Next2;

L_Next2:
    mesn;
    mes "\"Oh, ok.";
    mes "Have you already seen Hurnscald? It's a very exciting place! I love the entertainer. She can make so many funny faces.\"";
    close;

L_Done:
    mesn;
    mes "\"Hello! It was fun to play with you.";
    mes "When you go to the Hurnscald, you can say hello to Mikhail. He's very nice! But very shy.\"";
    close;
}

029-1,50,105,0	script	Latif	NPC262,{
    @child_number = 1;
    callfunc "XmasList";
    .@Exp = 15;

    .@q=getq(CandorQuest_HideNSeek);
    .@q2=getq2(CandorQuest_HideNSeek);
    if (.@q2 > 63) goto L_Done;
    if (.@q && !(.@q2 & $@hideandseekNT_found1)) goto L_Find;
    if (.@q) goto L_Doing;

    mes "You see a kid hiding behind a box.";
    menu
        "What are you doing there?",L_Next,
        "(Leave)",L_close;

L_Next:
    mes "[Latif]";
    mes "\"Hiding. We're playing hide and seek. Don't say that is boring. It's fun.\"";
    menu
        "I agree, it's fun.",L_Agree,
        "Hide and seek? Lame.",L_Next1;

L_Next1:
    mes "[Latif]";
    mes "\"My friend Vincent thinks so too. He doesn't join us playing.";
    mes "Do you know what he does instead? He builds action figures out of Bug Legs! That is lame.\"";
    goto L_close;

L_Agree:
    mes "[Latif]";
    mes "\"Yeah, right? My friend Vincent doesn't thinks so. He doesn't join us playing.";
    mes "Do you know what he does instead? He builds action figures out of Bug Legs! That is lame.\"";
    goto L_close;

L_Find:
    mes "[Latif]";
    mes "\"Hey! Ah, you found me. Well done. But my hiding place wasn't that difficult this time.\"";
    getexp .@Exp, 0;
    UpdateHideNSeek($@hideandseekNT_found1);
    mes "\"Here, do you want a candy?\"";
    getitem "Candy", 1;
    goto L_close;

L_Doing:
    mes "[Latif]";
    mes "\"You didn't find all of us yet? Seems like the other ones found a better place to hide.\"";
    mesq l("%d out of 5 have been found.", CountHideNSeek());
    goto L_close;

L_Done:
    mes "[Latif]";
    mes "\"The northeastern most building of town is a workshop. My father works there and wants me to do that too when I'm grown up. But I don't want do that! I want to go on an adventure! Just like you.\"";
    goto L_close;

L_close:
    close;
}

029-1,48,67,0	script	Charda	NPC260,{
    @child_number = 2;
    callfunc "XmasList";
    .@Exp = 15;

    .@q=getq(CandorQuest_HideNSeek);
    .@q2=getq2(CandorQuest_HideNSeek);
    if (.@q2 > 63) goto L_Done;
    if (.@q && !(.@q2 & $@hideandseekNT_found2)) goto L_Find;
    if (.@q) goto L_Doing;

    mes "[Charda]";
    mes "\"Hello. Psht, be careful. I'm playing hide and seek with Ayasha. You'll draw attention to my hiding place.\"";
    mes "\"They will never find me here.\"";
    goto L_close;

L_Find:
    mes "[Charda]";
    mes "\"Oh! Got me! You're good at this.\"";
    getexp .@Exp, 0;
    UpdateHideNSeek($@hideandseekNT_found2);
    mes "She takes something out of her pocket.";
    mes "\"I'm hungry now. Do you want chocolate too?\"";
    getitem "ChocolateBar", 1;
    mes "\"My mom doesn't want me to eat so much chocolate, but I got it from the trader at the Hurnscald Market. He sells many different things";
    mes "when we goto vacation in Hurnscald. I convinced him to teach me how to trade. It's fun.\"";
    goto L_close;

L_Doing:
    mes "[Charda]";
    mes "\"Did you find the others yet?\"";
    mesq l("Hrmm, no looks like only %d out of 5 have been found.", CountHideNSeek());
    goto L_close;

L_Done:
    mes "[Charda]";
    mes "\"Hey! We're still playing hide and seek. What are you going to do? Will you go to Hurnscald with the ferry? The mother of my friends Rossy and Julia come from there. They are on a vacation there at the moment.\"";
    mes "\"Rossy told me that it's a very beautiful place. Lots of trees and flowers and grass, everything's green.\"";
    mes "She sighs.";
    mes "[Charda]";
    mes "\"Sounds wonderful.\"";
    goto L_close;

L_close:
    close;
}

029-1,100,36,0	script	Faris	NPC259,{
    @child_number = 3;
    callfunc "XmasList";
    .@Exp = 25;

    .@q=getq(CandorQuest_HideNSeek);
    .@q2=getq2(CandorQuest_HideNSeek);
    if (.@q2 > 63) goto L_Done;
    if (.@q && !(.@q2 & $@hideandseekNT_found3)) goto L_Find;
    if (.@q) goto L_Doing;

    mes "[Faris]";
    mes "\"Oh, hi there. What are you doing here on the beach?\"";
    menu
        "I'm just having a look around.",L_Next,
        "It's nice place here, isn't it?",L_Next;

L_Next:
    mes "[Faris]";
    mes "\"Hey, you're cool. I'm hiding here from Ayasha, we're playing hide and seek. This is the best hiding place ever.\"";
    goto L_close;

L_Find:
    mes "[Faris]";
    mes "\"You found me! Well done. You look a bit thirsty, here.\"";
    getexp .@Exp, 0;
    getitem CactusDrink, 1;
    UpdateHideNSeek($@hideandseekNT_found3);
    mes "\"It's really cool you're playing with us. I'm sure you have a lot of other things to do.";
    mes "You're an adventurer, right? I want to become an adventurer too when I'm grown up. I'll talk to Aidan then.\"";
    mes "\"Aidan is outside Sorfina's place and you can register there for monster points. That's something the Wizard's Council made up. They reward you for slaying monsters. It's so cool.\"";
    goto L_close;

L_Doing:
    mes "[Faris]";
    mes "\"Are you still searching for the others? Nobody else is here.\"";
    mesq l("It seems you found %d out of 5 of my friends.", CountHideNSeek());
    goto L_close;

L_Done:
    mes "[Faris]";
    mes "\"I'll go to Hurnscald's Inn and ask Bernard for some soup when we're done with playing. His soup is delicious!\"";
    goto L_close;

L_close:
    close;
}

029-1,28,52,0	script	Rasin	NPC263,{
    @child_number = 4;
    callfunc "XmasList";

    .@Exp = 15;

    .@q=getq(CandorQuest_HideNSeek);
    .@q2=getq2(CandorQuest_HideNSeek);
    if (.@q2 > 63) goto L_Done;
    if (.@q && !(.@q2 & $@hideandseekNT_found4)) goto L_Find;
    if (.@q) goto L_Doing;

    mes "[Rasin]";
    mes "\"The harbor is to the south from here. There you can take a ferry to other places.";
    mes "And now go away, I'm playing hide and seek and Ayasha shouldn't see me here. You'll draw her attention when you talk to me.\"";
    goto L_close;

L_Find:
    mes "[Rasin]";
    mes "\"Ah, you found me. I need to find a better place to hide next time.\"";
    getexp .@Exp, 0;
    UpdateHideNSeek($@hideandseekNT_found4);
    goto L_close;

L_Doing:
    mes "[Rasin]";
    mes "\"Hehe, I'm curious when you'll have found all of us.\"";
    mesq l("There are %d out of 5 found.", CountHideNSeek());
    goto L_close;

L_Done:
    mes "[Rasin]";
    mes "\"Thanks for playing with us! It was fun!";
    mes "Will you leave Candor to go on an adventure in other places of the world?\"";
    goto L_close;

L_close:
    close;
}

029-1,79,99,0	script	Ghada	NPC265,{
    @child_number = 5;
    callfunc "XmasList";

    .@Exp = 15;

    .@q=getq(CandorQuest_HideNSeek);
    .@q2=getq2(CandorQuest_HideNSeek);
    if (.@q2 > 63) goto L_Done;
    if (.@q && !(.@q2 & $@hideandseekNT_found5)) goto L_Find;
    if (.@q) goto L_Doing;

    mes "You see a girl standing behind the wheat. She has a look of intense concentration on her face.";
    menu
        "Hi, what are you doing there?",L_Next,
        "(Leave)",L_close;

L_Next:
    mes "[Ghada]";
    mes "\"Oh, hello. I'm hiding. We're playing hide and seek.\"";
    goto L_close;

L_Find:
    mes "[Ghada]";
    mes "\"Oh no! You found me! You must be very clever.";
    mes "My friend Sandra is clever too, but she said it's a waste of time to play hide and seek.\"";
    getexp .@Exp, 0;
    UpdateHideNSeek($@hideandseekNT_found5);
    goto L_close;

L_Doing:
    mes "[Ghada]";
    mes "\"Are you still searching for the others? I'm sure you'll find them soon.\"";
    mesq l("You found %d out of 5 us.", CountHideNSeek());
    goto L_close;

L_Done:
    mes "[Ghada]";
    mes "\"It's so nice that you played with us! You're about the same age as my older sister, but she never plays with us. Her name is Sarah.\"";
    goto L_close;

L_close:
    close;
}
