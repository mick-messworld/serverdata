

017-9,27,28,0	script	#TravelConfig	NPC32767,{
    end;

OnInit:
    // TravelFound
    $@tut_bit = (1 << 2);
    //set $@druid_tree_bit, (1 << 3);
    $@graveyard_bit = (1 << 4);
    $@magic_house_bit = (1 << 5);
    $@terranite_cave_bit = (1 << 6);
    $@tulimshar_bit = (1 << 7);
    $@blue_sage_bit = (1 << 8);
    $@hurnscald_bit = (1 << 9);
    $@nivalis_bit = (1 << 10);
    //set $@tul_mine_bit, (1 << 11);
    $@pachua_bit = (1 << 12);
    $@barbarians_bit = (1 << 14);
    $@hurns_farms_bit = (1 << 15);
    $@candor_bit = (1 << 16);
    // Travel Base Cost
    $@tulimshar_cost = 100;
    $@hurnscald_cost = 100;
    $@nivalis_cost = 100;
    //set $@druid_tree_cost, 150;
    //set $@tul_mine_cost, 150;
    $@pachua_cost = 200;
    $@graveyard_cost = 200;
    $@magic_house_cost = 150;
    $@terranite_cave_cost = 200;
    $@blue_sage_cost = 200;
    $@barbarians_cost = 150;
    $@hurns_farms_cost = 150;
    $@candor_cost = 200;
    end;
}

function	script	Traveler	{
    if(@npcname$ == "") set @npcname$, strnpcinfo(1);
    mes "["+@npcname$+"]";
    mes "\"Greetings. I am "+@npcname$+" the Traveler.\"";
    next;

    if (TravelFound & $@tut_bit)
        goto L_Main;
    goto L_TravelTut;

L_Main:
    if (TravelFound & @NpcTravelBit)
        goto L_BitTravelSet;
    goto L_SetTravelBit;

L_BitTravelSet:
    @Cost = 10;
    if (BaseLevel < 45)
        @Cost = 5;
    goto L_Start;

L_TravelTut:
    mes "["+@npcname$+"]";
    mes "\"We travelers are found all over the world. Once you have found a traveler at a certain location, you can be sent back there instantly by another traveler.\"";
    next;
    if (TravelFound & $@tut_bit)
        goto L_Main;
    goto L_SetBit;

L_SetBit:
    TravelFound = TravelFound | $@tut_bit;
    goto L_Main;

L_SetTravelBit:
    mes "["+@npcname$+"]";
    mes "\"Uplink set. You can now return to this spot for a fee.\"";
    next;
    TravelFound = TravelFound | @NpcTravelBit;
    goto L_BitTravelSet;

L_Start:
    mes "\"Where would you like to go?\"";
    menu
        "Tonori - Tulimshar (" + (@Cost * $@tulimshar_cost) + " GP)", L_TravelTulimshar,
        "Argeas - Hurnscald (" + (@Cost * $@hurnscald_cost) + " GP)", L_TravelHurnscald,
        "Kaizei - Nivalis (" + (@Cost * $@nivalis_cost) + " GP)", L_TravelNivalis,
        "Tonori - Pachua's Village (" + (@Cost * $@pachua_cost) + " GP)", L_TravelPachua,
        "Argeas - Candor (" + (@Cost * $@candor_cost) + " GP)", L_TravelCandor,
        "Argeas - Magic House (" + (@Cost * $@magic_house_cost) + " GP)", L_TravelMagicHouse,
        "Argeas - Farmsteads (" + (@Cost * $@hurns_farms_cost) + " GP)", L_TravelHurnsFarms,
        "Argeas - Graveyard (" + (@Cost * $@graveyard_cost) + " GP)", L_TravelGraveyard,
        "Argeas - Terranite Cave (" + (@Cost * $@terranite_cave_cost) + " GP)", L_TravelTerranite,
        "Kaizei - Barbarian Village (" + (@Cost * $@barbarians_cost) + " GP)", L_TravelBarbarians,
        "Kaizei - Sage Nikolai's Mansion (" + (@Cost * $@blue_sage_cost) + " GP)", L_TravelBlueSage,
        "Who are the Travelers?", L_TravelTut,
        "I'm not interested.", L_TravelNo;

L_TravelChecks:
    if (@NpcTravelBit == @NextLocationBit)
        goto L_AlreadyThere;
    if (!(TravelFound & @NextLocationBit))
        goto L_NoFound;
    if (Zeny < @NextLocationCost)
        goto L_NoMoney;
    goto L_TravelPlayer;

L_TravelPlayer:
    mes "["+@npcname$+"]";
    mes "\"Be fearless!\"";
    close2;
    Zeny = Zeny - @NextLocationCost;
    warp @NextLocationMap$,@NextLocationX,@NextLocationY;
    goto L_Clearvars;

L_TravelGraveyard:
    @NextLocationBit = $@graveyard_bit;
    @NextLocationCost = (@Cost * $@graveyard_cost);
    @NextLocationMap$ = "026-1";
    @NextLocationX = 49;
    @NextLocationY = 45;
    goto L_TravelChecks;

L_TravelMagicHouse:
    @NextLocationBit = $@magic_house_bit;
    @NextLocationCost = (@Cost * $@magic_house_cost);
    @NextLocationMap$ = "013-1";
    @NextLocationX = 120;
    @NextLocationY = 93;
    goto L_TravelChecks;

L_TravelTerranite:
    @NextLocationBit = $@terranite_cave_bit;
    @NextLocationCost = (@Cost * $@terranite_cave_cost);
    @NextLocationMap$ = "012-3";
    @NextLocationX = 445;
    @NextLocationY = 65;
    goto L_TravelChecks;

L_TravelTulimshar:
    @NextLocationBit = $@tulimshar_bit;
    @NextLocationCost = (@Cost * $@tulimshar_cost);
    @NextLocationMap$ = "002-1";
    @NextLocationX = 60;
    @NextLocationY = 42;
    goto L_TravelChecks;

L_TravelBlueSage:
    @NextLocationBit = $@blue_sage_bit;
    @NextLocationCost = (@Cost  * $@blue_sage_cost);
    @NextLocationMap$ = "048-2";
    @NextLocationX = 26;
    @NextLocationY = 47;
    goto L_TravelChecks;

L_TravelHurnscald:
    @NextLocationBit = $@hurnscald_bit;
    @NextLocationCost = (@Cost * $@hurnscald_cost);
    @NextLocationMap$ = "008-1";
    @NextLocationX = 79;
    @NextLocationY = 84;
    goto L_TravelChecks;

L_TravelNivalis:
    @NextLocationBit = $@nivalis_bit;
    @NextLocationCost = (@Cost * $@nivalis_cost);
    @NextLocationMap$ = "020-1";
    @NextLocationX = 53;
    @NextLocationY = 122;
    goto L_TravelChecks;

L_TravelPachua:
    @NextLocationBit = $@pachua_bit;
    @NextLocationCost = (@Cost * $@pachua_cost);
    @NextLocationMap$ = "006-1";
    @NextLocationX = 28;
    @NextLocationY = 97;
    callfunc "MiriamCheat";
    goto L_TravelChecks;

L_TravelBarbarians:
    @NextLocationBit = $@barbarians_bit;
    @NextLocationCost = (@Cost * $@barbarians_cost);
    @NextLocationMap$ = "033-1";
    @NextLocationX = 66;
    @NextLocationY = 33;
    goto L_TravelChecks;

L_TravelHurnsFarms:
    @NextLocationBit = $@hurns_farms_bit;
    @NextLocationCost = (@Cost * $@hurns_farms_cost);
    @NextLocationMap$ = "055-1";
    @NextLocationX = 135;
    @NextLocationY = 60;
    goto L_TravelChecks;

L_TravelCandor:
    @NextLocationBit = $@candor_bit;
    @NextLocationCost = (@Cost * $@candor_cost);
    @NextLocationMap$ = "029-1";
    @NextLocationX = 69;
    @NextLocationY = 69;
    goto L_TravelChecks;

L_TravelNo:
    mes "["+@npcname$+"]";
    mes "\"Perhaps you will have the courage to help us some day.\"";
    close2;
    goto L_Clearvars;

L_NoMoney:
    mes "["+@npcname$+"]";
    mes "\"I'm sorry, but you don't have enough money. Maybe next time.\"";
    close2;
    goto L_Clearvars;

L_NoFound:
    mes "["+@npcname$+"]";
    mes "\"Sorry, but you haven't visited a traveler yet at that location. You should find and talk to a traveler there so you can quickly return to that location in the future.\"";
    close2;
    goto L_Clearvars;

L_AlreadyThere:
    mes "["+@npcname$+"]";
    mes "\"Uh... You're already here. Are you sure you know where you are going?\"";
    close2;
    goto L_Clearvars;

L_Clearvars:
    @npcname$ = "";
    @Cost = 0;
    @NextLocationBit = 0;
    @NextLocationCost = 0;
    @NextLocationMap$ = "";
    @NextLocationX = 0;
    @NextLocationY = 0;
    return;
}
