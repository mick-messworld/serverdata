
017-9,27,28,0	script	#FerryConfig	NPC32767,{
    end;

OnInit:
    disablenpc "Hurnscald South Koga";
    disablenpc "Candor Koga";
    disablenpc "Nivalis Koga";
    disablenpc "Hurnscald North Koga";
    disablenpc "Tulimshar Koga";
    $@MainCurrentDock = 0;
    $@CandorCurrentDock = 0;
    $@DockTickCount = 0;
    $@DockLeaveCount = 1;
    setarray $@MainDocks$, "Hurnscald North", "Nivalis", "Tulimshar";
    setarray $@CandorDocks$, "Candor", "Hurnscald South";
    donpcevent "#"+$@MainDocks$[$@MainCurrentDock]+"Dock::OnCommandArrive";
    donpcevent "#"+$@CandorDocks$[$@CandorCurrentDock]+"Dock::OnCommandArrive";
    .warp_delay = 430;
    if (debug >= 2) end;
    initnpctimer;
    goto L_k1city2;

L_NextDock:
    $@MainLastDock = $@MainCurrentDock;
    $@CandorLastDock = $@CandorCurrentDock;
    $@MainCurrentDock = $@MainCurrentDock + 1;
    $@CandorCurrentDock = $@CandorCurrentDock + 1;
    if($@MainCurrentDock == getarraysize($@MainDocks$))
        $@MainCurrentDock = 0;
    if($@CandorCurrentDock == getarraysize($@CandorDocks$))
        $@CandorCurrentDock = 0;
    $@DockTickCount = 0;
    donpcevent "#"+$@MainDocks$[$@MainLastDock]+"Dock::OnCommandWarp";
    donpcevent "#"+$@MainDocks$[$@MainCurrentDock]+"Dock::OnCommandArrive";
    donpcevent "#"+$@CandorDocks$[$@CandorLastDock]+"Dock::OnCommandWarp";
    donpcevent "#"+$@CandorDocks$[$@CandorCurrentDock]+"Dock::OnCommandArrive";

    donpcevent "#k1sound::OnCommandDing";
    donpcevent "#k2sound::OnCommandDing";

    initnpctimer;

    if($@MainCurrentDock == 2) goto L_k1city1;
    if($@MainCurrentDock == 0) goto L_k1city2;
    if($@MainCurrentDock == 1) goto L_k1city3;
    end;

OnTimer5000:
    if ($@DockTickCount > $@DockLeaveCount)
        goto L_NextDock;
    $@DockTickCount = $@DockTickCount + 1;
    initnpctimer;
    end;


L_k1city1:
  enablenpc "#k1city1e"; disablenpc "#k1city2e"; disablenpc "#k1city3e";
  disablenpc "#k1city1d"; enablenpc "#k1city2d"; enablenpc "#k1city3d";
  if($@CandorCurrentDock == 1) goto L_k2city1;
  if($@CandorCurrentDock == 0) goto L_k2city2;
end;

L_k1city2:
  disablenpc "#k1city1e"; enablenpc "#k1city2e"; disablenpc "#k1city3e";
  enablenpc "#k1city1d"; disablenpc "#k1city2d"; enablenpc "#k1city3d";
  if($@CandorCurrentDock == 1) goto L_k2city1;
  if($@CandorCurrentDock == 0) goto L_k2city2;
end;

L_k1city3:
  disablenpc "#k1city1e"; disablenpc "#k1city2e"; enablenpc "#k1city3e";
  enablenpc "#k1city1d"; enablenpc "#k1city2d"; disablenpc "#k1city3d";
  if($@CandorCurrentDock == 1) goto L_k2city1;
  if($@CandorCurrentDock == 0) goto L_k2city2;
end;

L_k2city1:
  enablenpc "#k2city1e"; disablenpc "#k2city2e";
  disablenpc "#k2city1d"; enablenpc "#k2city2d";
end;

L_k2city2:
  disablenpc "#k2city1e"; enablenpc "#k2city2e";
  enablenpc "#k2city1d"; disablenpc "#k2city2d";
end;
}

function	script	FerryHelp	{
    mes "\"You wait on the dock for the ship to come in. You'll be given a chance to board the boat when it comes into port.\"";
    mes "\"It lingers in port to allow you some time to board in case you are running behind.\"";
    mes "\"Once on the ship, it will sail to different ports and annouce where it is docking.\"";
    mes "\"There are 2 Ferrys, both ferrys are free to ride.\"";
    mes "\"The Main ferry travels from Argeas, Kaizei and Tonori. It docks in the major ports Hurnscald North, Nivalis and the Tulimshar Dock.\"";
    mes "\"The Candor Ferry only travels betwen Candor and the Hurnscald South Dock.\"";
    mes "\"Refreshments and supplies are offered aboard both ships during the voyage.\"";
    mes "\"We also have some slot machines in case you get bored.\"";
    return;
}

function	script	BoardFerry	{
    if   ($@MainCurrentDock == 0 && getmapname() == "008-1") goto L_Board;
    else if ($@MainCurrentDock == 1 && getmapname() == "031-1") goto L_Board;
    else if ($@MainCurrentDock == 2 && getmapname() == "001-1") goto L_Board;

    @NextDock = $@MainCurrentDock + 1;
    if(@NextDock == getarraysize($@MainDocks$)) set @NextDock, 0;
    message strcharinfo(0),
            "Ferry : ##3The ferry is currently at ##B"+$@MainDocks$[$@MainCurrentDock]+"##b. "
            +"It will be arriving at ##B"+$@MainDocks$[@NextDock]+"##b next.";
    return;

L_Board:
    warp "035-2",32,29;
    return;
}

function	script	BoardCandorFerry	{
    if   ($@CandorCurrentDock == 0 && getmapname() == "029-1") goto L_Board;
    else if ($@CandorCurrentDock == 1 && getmapname() == "008-1") goto L_Board;

    @NextDock = $@CandorCurrentDock + 1;
    if(@NextDock == getarraysize($@CandorDocks$)) set @NextDock, 0;
    message strcharinfo(0),
            "Ferry : ##3The ferry is currently at ##B"+$@CandorDocks$[$@CandorCurrentDock]+"##b. "
            +"It will be arriving at ##B"+$@CandorDocks$[@NextDock]+"##b next.";
    return;

L_Board:
    warp "036-2",32,29;
    goto L_Return;

L_Return:
    return;
}
