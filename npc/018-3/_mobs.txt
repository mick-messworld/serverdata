// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 018-3: Northern Mines mobs
018-3,48,29,28,9	monster	Bat	1017,10,100000,30000
018-3,35,144,15,24	monster	Evil Mushroom	1013,10,120000,30000
018-3,35,65,14,11	monster	Spider	1012,20,100000,50000
018-3,28,157,1,1	monster	Pink Flower	1014,1,60000,30000
018-3,99,111,8,20	monster	Yellow Slime	1007,10,90000,30000
018-3,134,140,6,4	monster	Maggot	1002,3,80000,40000
018-3,82,149,36,13	monster	Spider	1012,8,300000,120000
018-3,102,172,65,6	monster	Black Scorpion	1009,20,60000,10000
018-3,174,37,5,15	monster	Yellow Slime	1007,5,180000,120000
018-3,148,148,6,12	monster	Bat	1017,3,180000,40000
018-3,136,87,10,3	monster	Yellow Slime	1007,6,80000,40000
018-3,81,77,13,10	monster	Red Slime	1008,15,80000,40000
018-3,111,80,2,3	monster	Black Scorpion	1009,10,80000,40000
018-3,175,122,2,20	monster	Snake	1010,15,40000,20000
018-3,172,94,8,8	monster	Red Slime	1008,10,60000,30000
018-3,59,95,5,4	monster	Spider	1012,15,60000,30000
018-3,77,92,11,3	monster	Black Scorpion	1009,15,60000,30000
018-3,94,55,8,4	monster	Snake	1010,5,60000,30000
018-3,133,39,6,8	monster	Spider	1012,10,60000,30000
018-3,111,33,15,8	monster	Snake	1010,15,60000,30000
018-3,68,101,2,2	monster	Cave Snake	1021,4,60000,30000
018-3,153,96,9,8	monster	Cave Maggot	1056,4,100000,30000
018-3,99,99,8,8	monster	Cave Maggot	1056,4,100000,30000
