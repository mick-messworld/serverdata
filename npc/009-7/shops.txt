// The Mana World Script
// (C) Jesusalva, 2021
// Licensed under GPLv2 or later

009-7,34,23,0	script	Bartender#Duels	NPC177,{
    shop .name$;
    goodbye;
    close;

OnInit:
    tradertype(NST_ZENY);
    sellitem Beer;
    sellitem IronPotion;
    sellitem ConcentrationPotion;
    sellitem BottleOfWater;
    sellitem Milk;
    .distance = 5;
    end;
}

009-7,27,26,0	script	Garcon#Duels	NPC180,{
    shop .name$;
    goodbye;
    close;

OnInit:
    tradertype(NST_ZENY);
    sellitem RoastedMaggot;
    sellitem PickledBeets;
    sellitem ChickenLeg;
    sellitem Steak;
    sellitem Beer;
    .distance = 5;
    end;
}

